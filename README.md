# Meteor-Angular-WebApp

### Build Status

### Setup
- Clone submodules. 
    - `git submodule update --init`
- Install / Update API node modules.
    -  `cd api && meteor npm i && cd ..` 
- Install / Update Webapp node modules.
    -  `npm i`     
### Serve
- Serve webapp.
    - `npm serve:webapp`
- Serve api.
    - `npm serve:api`
- Serve webapp and api both.
    - `npm start`
