import { MongoObservable } from 'meteor-rxjs';
import { Todo } from '../models/models';

export const todosCollection = new MongoObservable.Collection<Todo>('todos');
