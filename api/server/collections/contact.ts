import { MongoObservable } from 'meteor-rxjs';
import { Contact } from '../models/contact';

export const contactCollection = new MongoObservable.Collection<Contact>('contact');
