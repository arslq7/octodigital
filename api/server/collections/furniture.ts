import { MongoObservable } from 'meteor-rxjs';
import { Furniture } from '../models/furniture';

export const furnitureCollection = new MongoObservable.Collection<Furniture>('furniture');
