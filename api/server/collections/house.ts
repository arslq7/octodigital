import { MongoObservable } from 'meteor-rxjs';
import { House } from '../models/house';

export const houseCollection = new MongoObservable.Collection<House>('house');
