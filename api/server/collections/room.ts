import { MongoObservable } from 'meteor-rxjs';
import { Room } from '../models/room';

export const roomCollection = new MongoObservable.Collection<Room>('room');
