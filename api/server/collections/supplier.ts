import { MongoObservable } from 'meteor-rxjs';
import { Supplier } from '../models/supplier';

export const supplierCollection = new MongoObservable.Collection<Supplier>('supplier');
