import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { contactCollection } from '../collections/contact';
import { houseCollection } from '../collections/house';
import { Contact } from '../models/contact';
import { House } from '../models/house';
import { Room } from '../models/room';
import { Supplier } from '../models/supplier';

Meteor.methods({
  addContact(contact: Contact): Observable<any> {
    return contactCollection.insert(contact);
  },
  async deleteContact(contact: Contact): Promise<any> {
    try {
      await contactCollection.remove(contact._id);
    } catch (e) {
      throw new Meteor.Error('Unable To Delete contact !');
    }
  },
});
