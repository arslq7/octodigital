import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { furnitureCollection } from '../collections/furniture';
import { houseCollection } from '../collections/house';
import { roomCollection } from '../collections/room';
import { Furniture } from '../models/furniture';
import { House } from '../models/house';
import { Room } from '../models/room';

Meteor.methods({
  addFurniture(furniture: Furniture): Observable<any> {
    return furnitureCollection.insert(furniture);
  },
  async deleteFurniture(furniture: Furniture): Promise<void> {
    try {
      await furnitureCollection.remove(furniture._id);
    } catch (e) {
      throw new Meteor.Error('Unable to remove Furniture');
    }
  },
  async updateFurniture(house: House): Promise<any> {
    try {
      await houseCollection.update(house._id, house);
    } catch (e) {
      throw new Meteor.Error('Unable to add furniture in room');
    }
  }
});
