import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { houseCollection } from '../collections/house';
import { House } from '../models/house';
import { Room } from '../models/room';

Meteor.methods({
  addHouse(house: House): Observable<any> {
    return houseCollection.insert(house);
  },
  async deleteHouse(house: House): Promise<void> {
    try {
      await houseCollection.remove(house._id);
    } catch (e) {
      throw new Meteor.Error('Unable To Delete House !');
    }
  },
  async addRoomInHouse(house: House, room: Room): Promise<any> {
    try {
      await houseCollection.update({_id: house._id}, {$addToSet: {rooms: room}});
    } catch (e) {
      throw new Meteor.Error('Unable to add room in house');
    }
  },
  async removeRoomFromHouse(house: House, room: Room): Promise<any> {
    try {
      await houseCollection.update({_id: house._id}, {$pull: {rooms: {_id: room._id}}});
    } catch (e) {
      throw new Meteor.Error('Unable to remove room from House');
    }
  },
  async updateHouse(house: House): Promise<any> {
    try {
      await houseCollection.update(house._id, house);
    } catch (e) {
      throw new Meteor.Error('Unable to remove room from House');
    }
  }

});
