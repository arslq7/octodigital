import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { todosCollection } from '../collections/collections';

Meteor.methods({
  addTodo(content: string): Observable<string> {
    check(content, String);

    return todosCollection.insert({
      content
    });
  }
});
