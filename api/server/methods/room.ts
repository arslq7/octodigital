import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { roomCollection } from '../collections/room';
import { Room } from '../models/room';

Meteor.methods({
  addRoom(room: Room): Observable<any> {
    return roomCollection.insert(room);
  },
  async deleteRoom(room: Room): Promise<any> {
    try {
      await roomCollection.remove(room._id);
    } catch (e) {
      throw new Meteor.Error('Unable To Delete Room !');
    }
  }
})
