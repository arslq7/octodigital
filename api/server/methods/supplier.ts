import { Meteor } from 'meteor/meteor';
import { Observable } from 'rxjs';
import { houseCollection } from '../collections/house';
import { supplierCollection } from '../collections/supplier';
import { Contact } from '../models/contact';
import { House } from '../models/house';
import { Room } from '../models/room';
import { Supplier } from '../models/supplier';

Meteor.methods({
  addSupplier(supplier: Supplier): Observable<any> {
    return supplierCollection.insert(supplier);
  },
  async deleteSupplier(supplier: Supplier): Promise<any> {
    try {
      await supplierCollection.remove(supplier._id);
    } catch (e) {
      throw new Meteor.Error('Unable To Delete Supplier !');
    }
  },
  async addContactInSupplier(supplier: Supplier, contact: Contact): Promise<any> {
    try {
      await supplierCollection.update({_id: supplier._id}, {$addToSet: {contacts: contact}});
    } catch (e) {
      throw new Meteor.Error('Unable to add contact in supplier');
    }
  },
  async removeContactFromSupplier(supplier: Supplier, contact: Contact): Promise<any> {
    try {
      await supplierCollection.update({_id: supplier._id}, {$pull: {contacts: {_id: contact._id}}});
    } catch (e) {
      throw new Meteor.Error('Unable to remove supplier from supplier');
    }
  }
});
