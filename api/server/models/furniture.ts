export interface Furniture {
  _id?: string;
  name?: string;
  materials?: [];
}
