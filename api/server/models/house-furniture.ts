export interface HouseFurniture {
  _id?: string;
  house: string;
  room: string;
  furniture: string;
}
