export interface House {
  _id?: string;
  name?: string;
  rooms?: [];
  materials?: [];
}
