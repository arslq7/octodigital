export interface Material {
  _id?: string;
  name?: string;
  suppliers?: [];
}
