import { Meteor } from 'meteor/meteor';
import { contactCollection } from '../collections/contact';

Meteor.publish('contact', () => contactCollection.find());
