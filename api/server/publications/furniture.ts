import { Meteor } from 'meteor/meteor';
import { furnitureCollection } from '../collections/furniture';

Meteor.publish('furniture', () => furnitureCollection.find());
