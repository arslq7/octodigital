import { Meteor } from 'meteor/meteor';
import { houseCollection } from '../collections/house';

Meteor.publish('house', () => houseCollection.find());
