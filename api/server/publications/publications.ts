import { Meteor } from 'meteor/meteor';
import { todosCollection } from '../collections/collections';

Meteor.publish('todos', () => todosCollection.find());
