import { Meteor } from 'meteor/meteor';
import { roomCollection } from '../collections/room';

Meteor.publish('room', () => roomCollection.find());
