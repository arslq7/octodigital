import { Meteor } from 'meteor/meteor';
import { supplierCollection } from '../collections/supplier';

Meteor.publish('supplier', () => supplierCollection.find());
