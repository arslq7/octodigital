import { browser, by, element, promise } from 'protractor';

export class AppPage {
  async navigateTo(): promise.Promise<any> {
    return browser.get('/');
  }

  async getParagraphText(): promise.Promise<string> {
    return element(by.css('app-root h1'))
      .getText();
  }
}
