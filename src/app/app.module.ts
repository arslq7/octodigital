import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { InputTextModule } from 'primeng/inputtext';


import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { AppComponent } from './app.component';
import { HouseComponent } from './house/house.component';
import { RoomsComponent } from './rooms/rooms.component';

import { RoutingModule } from './routing.module';
import { TestComponent } from './test-component/test.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SupplierComponent } from './supplier/supplier.component';
import { MaterialComponent } from './material/material.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    HouseComponent,
    RoomsComponent,
    DashboardComponent,
    SupplierComponent,
    MaterialComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    ButtonModule,
    ToastModule,
    InputTextModule,
    DropdownModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
}
