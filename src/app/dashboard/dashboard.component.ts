import { Component, NgZone, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { houseCollection } from '../../../api/server/collections/house';
import { House } from '../../../api/server/models/house';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  houses: Array<House>;
  constructor(
    private zone: NgZone
  ) { }

  ngOnInit(): void {
    MeteorObservable.subscribe('house')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.houses = houseCollection
              .find({})
              .fetch();
          });
        });
      });
  }

}
