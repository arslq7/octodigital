import { Component, NgZone, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { houseCollection } from '../../../api/server/collections/house';
import { roomCollection } from '../../../api/server/collections/room';
import { House } from '../../../api/server/models/house';
import { Room } from '../../../api/server/models/room';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.scss']
})
export class HouseComponent implements OnInit {
  houses: Array<House>;
  selectedHouse: House;
  rooms: Array<Room>;
  selectedRoom: Room;
  roomFlag = true;
  houseFlag = true;
  newHouse = '';
  newRoom = '';
  constructor(
    private zone: NgZone
  ) {
  }

  ngOnInit(): void {
    MeteorObservable.subscribe('house')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.houses = houseCollection
              .find({})
              .fetch();
            if (this.houses.length !== 0) {
              if (this.houseFlag) {
                this.selectedHouse = this.houses[0];
                this.houseFlag = false;
              } else if (!this.houseFlag) {
                const index = this.houses.findIndex(house => house._id === this.selectedHouse._id);
                this.selectedHouse = this.houses[index];
              }
            }
          });
        });
      });
    MeteorObservable.subscribe('room')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.rooms = roomCollection
              .find({})
              .fetch();
            if (this.rooms.length !== 0){
              if (this.roomFlag) {
                this.selectedRoom = this.rooms[0];
                this.roomFlag = false;
              } else if (!this.roomFlag) {
                const index = this.rooms.findIndex(room => room._id === this.selectedRoom._id);
                this.selectedRoom = this.rooms[index];
              }
            }
          });
        });
      });
  }
  moveToRoom(room: Room): void {
    MeteorObservable.call('removeRoomFromHouse', this.selectedHouse, room)
      .subscribe();
  }
  moveToHouseRooms(roomId): void {
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    const roomIndex = this.rooms.findIndex(room => room._id === roomId);
    MeteorObservable.call('addRoomInHouse', this.houses[houseIndex], this.rooms[roomIndex])
      .subscribe();

  }
  selectRoom(id): void {
    this.rooms.forEach(room => {
      if (room._id === id) {
        this.selectedRoom = room;
      }
    });
  }
  selectHouse(id): void {
    this.houses.forEach(house => {
      if (house._id === id) {
        this.selectedHouse = house;
      }
    });
    console.log(this.selectedHouse);
  }
  addNewHouse(): void {
    if (this.newHouse !== '') {
      const newHouse = {name: this.newHouse, rooms: [], materials: []};
      MeteorObservable.call('addHouse', newHouse)
        .subscribe();
      this.newHouse = '';
    }
  }
  addNewRoom(): void {
    if (this.newRoom !== '') {
      const newRoom = {name: this.newRoom, furnitures: [], materials: []};
      MeteorObservable.call('addRoom', newRoom)
        .subscribe();
      this.newRoom = '';
    }
  }
  deleteHouse(): void {
    if (this.selectedHouse) {
      MeteorObservable.call('deleteHouse', this.selectedHouse)
        .subscribe();
      this.houseFlag = true;
    }
  }
  deleteRoom(): void {
    if (this.selectedRoom) {
      MeteorObservable.call('deleteRoom', this.selectedRoom)
        .subscribe();
      this.roomFlag = true;
    }
  }
}
