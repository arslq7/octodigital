import { Component, NgZone, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { houseCollection } from '../../../api/server/collections/house';
import { supplierCollection } from '../../../api/server/collections/supplier';
import { Furniture } from '../../../api/server/models/furniture';
import { House } from '../../../api/server/models/house';
import { Room } from '../../../api/server/models/room';
import { Supplier } from '../../../api/server/models/supplier';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {
  houses: Array<House>;
  suppliers: Array<Supplier>;
  roomInHouse: Array<House> = [];
  furnitureInRoom: Array<Furniture> = [];
  selectedHouse: House;
  selectedSupplier: Supplier;
  selectedFurniture: Furniture;
  houseFlag = true;
  selectedOption = 'house';
  selectedRoom: Room;
  constructor(
    private zone: NgZone
  ) { }

  ngOnInit(): void {
    MeteorObservable.subscribe('house')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.houses = houseCollection
              .find({})
              .fetch();
            if (this.houseFlag) {
              if (this.houses.length > 0) {
                this.selectedHouse = this.houses[0];
                console.log(this.selectedHouse);
              } else {
                this.selectedHouse = {};
              }
              this.houseFlag = false;
            } else {
              const index = this.houses.findIndex(house => house._id === this.selectedHouse._id);
              this.selectedHouse = this.houses[index];
            }
            this.getRooms();
            this.getFurniture();
          });
        });
      });
    MeteorObservable.subscribe('supplier')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.suppliers = supplierCollection
              .find({})
              .fetch();
            if (this.suppliers.length > 0) {
              this.selectedSupplier = this.suppliers[0];
            }
          });
        });
      });
  }
  changeOption(option): void {
    this.selectedOption = option;
    console.log(this.roomInHouse);
  }
  resetArrays(): void {
    setTimeout(() => {
      console.log(this.selectedHouse);
      this.getRooms();
      this.getFurniture();
      this.selectedRoom = {};
    }, 10);
  }
  moveToHouse(supplierId): void {
    const supplier = this.selectedSupplier;
    // @ts-ignore
    const search = this.selectedHouse.materials.findIndex(mat => mat._id === supplier._id);
    if (search === -1) {
      // @ts-ignore
      this.selectedHouse.materials.push(supplier);
      MeteorObservable.call('updateHouse', this.selectedHouse)
        .subscribe();
    }
  }
  removeFromHouse(supplierId): void {
    // @ts-ignore
    const index = this.selectedHouse.materials.findIndex(mat => mat._id === supplierId);
    this.selectedHouse.materials.splice(index, 1);
    MeteorObservable.call('updateHouse', this.selectedHouse)
      .subscribe();
  }
  moveToRoom(supplierId): void {
    const newRoom = this.selectedRoom;
    const supplier = this.selectedSupplier;
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    // @ts-ignore
    const roomIndex = this.houses[houseIndex].rooms.findIndex(room => room._id === newRoom._id);
    // @ts-ignore
    const search = this.houses[houseIndex].rooms[roomIndex].materials.findIndex(mat => mat._id === supplierId);
    if (search === -1) {
      // @ts-ignore
      this.houses[houseIndex].rooms[roomIndex].materials.push(supplier);
      MeteorObservable.call('updateHouse', this.houses[houseIndex])
        .subscribe();
      this.selectedRoom = this.houses[houseIndex].rooms[roomIndex];
    }
  }
  removeFromRoom(supplierId): void {
    const oldRoom = this.selectedRoom;
    const supplier = this.selectedSupplier;
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    // @ts-ignore
    const roomIndex = this.houses[houseIndex].rooms.findIndex(room => room._id === oldRoom._id);
    // @ts-ignore
    const materialIndex = this.houses[houseIndex].rooms[roomIndex].materials.findIndex(mat => mat._id === supplierId);
    // @ts-ignore
    this.houses[houseIndex].rooms[roomIndex].materials.splice(materialIndex, 1);
    MeteorObservable.call('updateHouse', this.houses[houseIndex])
      .subscribe();
    this.selectedRoom = this.houses[houseIndex].rooms[roomIndex];
  }
  selectRoom(roomId): void {
    const index = this.roomInHouse.findIndex(room => room._id === roomId);
    this.selectedRoom = this.roomInHouse[index];
  }
  removeFromFurniture(supplierId): void {
    const furniture = this.selectedFurniture;
    const material = this.selectedSupplier;
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    const rooms = this.houses[houseIndex].rooms;
    rooms.forEach(room => {
      // @ts-ignore
      const roomSearch = room.furnitures.findIndex(fur => fur._id === furniture._id);
      if (roomSearch !== -1) {
        // @ts-ignore
        const materialIndex = room.furnitures[roomSearch].materials.findIndex(mat => mat._id === supplierId);
        // @ts-ignore
        room.furnitures[roomSearch].materials.splice(materialIndex, 1);
      }
    });
    this.selectedHouse.rooms = [];
    this.selectedHouse.rooms = rooms;
    MeteorObservable.call('updateHouse', this.houses[houseIndex])
      .subscribe();
    const furIndex = this.furnitureInRoom.findIndex(fur => fur._id === furniture._id);
    this.selectedFurniture = this.furnitureInRoom[furIndex];
  }
  moveToFurniture(supplierId): void {
    const furniture = this.selectedFurniture;
    // @ts-ignore
    const search = furniture.materials.findIndex(mat => mat._id === supplierId);
    if (search === -1) {
      const material = this.selectedSupplier;
      const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
      const rooms = this.houses[houseIndex].rooms;
      rooms.forEach(room => {
        // @ts-ignore
        const roomSearch = room.furnitures.findIndex(fur => fur._id === furniture._id);
        if (roomSearch !== -1) {
          // @ts-ignore
          room.furnitures[roomSearch].materials.push(material);
        }
      });
      this.selectedHouse.rooms = [];
      this.selectedHouse.rooms = rooms;
      MeteorObservable.call('updateHouse', this.selectedHouse)
        .subscribe();
      const furIndex = this.furnitureInRoom.findIndex(fur => fur._id === furniture._id);
      this.selectedFurniture = this.furnitureInRoom[furIndex];
    }
  }
  selectFurniture(furnitureId): void {
    const index = this.furnitureInRoom.findIndex(fur => fur._id === furnitureId);
    this.selectedFurniture = this.furnitureInRoom[index];
  }
  selectSupplier(supplierId): void {
    const index = this.suppliers.findIndex(supplier => supplier._id === supplierId);
    this.selectedSupplier = this.suppliers[index];
  }
  getRooms(): void {
    this.roomInHouse = [];
    const index = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    const rooms = this.houses[index].rooms;
    rooms.forEach(room => {
       if (this.roomInHouse === []) {
          this.roomInHouse.push(room);
       } else {
         // @ts-ignore
         const search = this.roomInHouse.findIndex(r => r._id === room._id);
         if (search === -1) {
           this.roomInHouse.push(room);
         }
       }
     });
  }
  getFurniture(): void {
    this.furnitureInRoom = [];
    const index = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    const rooms = this.houses[index].rooms;
    rooms.forEach(room => {
      // @ts-ignore
      const furniture = room.furnitures;
      if (furniture.length > 0) {
        for (let i = 0; i < furniture.length; i++) {
          if (this.furnitureInRoom === []) {
            this.furnitureInRoom.push(furniture[i]);
          } else {
            const search = this.furnitureInRoom.findIndex(fur => fur._id === furniture[i]._id);
            if (search === -1) {
              this.furnitureInRoom.push(furniture[i]);
            }
          }
        }
      }
    });
  }
}
