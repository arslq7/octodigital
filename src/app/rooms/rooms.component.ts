import { Component, NgZone, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { furnitureCollection } from '../../../api/server/collections/furniture';
import { houseCollection } from '../../../api/server/collections/house';
import { Furniture } from '../../../api/server/models/furniture';
import { House } from '../../../api/server/models/house';
import { Room } from '../../../api/server/models/room';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {
  houses: Array<House>;
  furnitures: Array<Furniture>;
  selectedHouse: House;
  selectedRoom: Room;
  selectedFurniture: Furniture;
  newFurniture = '';
  houseFlag = true;
  furnitureFlag = true;
  constructor(
    public zone: NgZone
  ) {
  }
  ngOnInit(): void {
    MeteorObservable.subscribe('house')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.houses = houseCollection
              .find({})
              .fetch();
            if (this.houseFlag) {
              this.selectedHouse = this.houses[0];
              if (this.houses[0].rooms.length >= 0) {
                // @ts-ignore
                this.selectedRoom = this.houses[0].rooms[0];
              }
              // @ts-ignore
              this.selectedRoom = this.houses[0].rooms[0];
              this.houseFlag = false;
            } else if (!this.houseFlag) {
              const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
              this.selectedHouse = this.houses[houseIndex];
              // @ts-ignore

              const roomIndex = this.houses[houseIndex].rooms.findIndex(room => room._id === this.selectedRoom._id);
              this.selectedRoom = this.houses[houseIndex].rooms[roomIndex];
            }
          });
        });
      });
    MeteorObservable.subscribe('furniture')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.furnitures = furnitureCollection
              .find({})
              .fetch();
            if (this.furnitures.length !== 0) {
              if (this.furnitureFlag) {
                this.selectedFurniture = this.furnitures[0];
                this.furnitureFlag = false;
              } else if (!this.furnitureFlag) {
                const index = this.furnitures.findIndex(furniture => furniture._id === this.selectedFurniture._id);
                this.selectedFurniture = this.furnitures[index];
              }
            }
          });
        });
      });
  }
  selectRoom(id): void {
    // @ts-ignore
    const index = this.selectedHouse.rooms.findIndex(room => room._id === id);
    this.selectedRoom = this.selectedHouse.rooms[index];
    console.log(this.selectedRoom);
  }
  moveToFurniture(furnitureId): void {
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    // @ts-ignore
    const roomIndex = this.houses[houseIndex].rooms.findIndex(room => room._id === this.selectedRoom._id);
    // @ts-ignore
    const furnitureIndex = this.houses[houseIndex].rooms[roomIndex].furnitures.findIndex(furniture => furniture._id === furnitureId);

    // const furnitureIndex = this.furnitures.findIndex(furniture => furniture._id === furnitureId);
    // @ts-ignore
    this.houses[houseIndex].rooms[roomIndex].furnitures.splice(furnitureIndex, 1);
    MeteorObservable.call('updateFurniture', this.houses[houseIndex])
      .subscribe();
  }
  moveToRoomFurniture(furniture): void {
    const houseIndex = this.houses.findIndex(house => house._id === this.selectedHouse._id);
    // @ts-ignore
    const roomIndex = this.houses[houseIndex].rooms.findIndex(room => room._id === this.selectedRoom._id);
    const house = this.houses[houseIndex];
    // @ts-ignore
    house.rooms[roomIndex].furnitures.push(furniture);
    MeteorObservable.call('updateFurniture', house)
      .subscribe();
  }
  selectFurniture(furnitureId): void {
    const index = this.furnitures.findIndex(fur => fur._id === furnitureId);
    this.selectedFurniture = this.furnitures[index];
  }
  addFurniture(): void {
    if (this.newFurniture !== '') {
      const furniture = {name: this.newFurniture, materials: []};
      MeteorObservable.call('addFurniture', furniture)
        .subscribe();
    }
    this.newFurniture = '';
  }
  deleteFurniture(): void {
    MeteorObservable.call('deleteFurniture', this.selectedFurniture)
      .subscribe();
    this.furnitureFlag = true;
  }
  removeSelected(): void {
    this.selectedRoom = {};
  }
}
