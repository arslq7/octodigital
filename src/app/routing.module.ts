import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HouseComponent } from './house/house.component';
import { MaterialComponent } from './material/material.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SupplierComponent } from './supplier/supplier.component';

const routes: Routes = [
  {
    path: '',
    component: HouseComponent
  },
  {
    path: 'rooms',
    component: RoomsComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'supplier',
    component: SupplierComponent
  },
  {
    path: 'material',
    component: MaterialComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class RoutingModule {
}
