import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    public message: MessageService
  ) { }
  toast(type: string, heading: string, message: string): void {
    this.message.add({severity:  type, summary: heading, detail: message});
  }
}
