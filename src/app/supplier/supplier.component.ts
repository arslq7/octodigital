import { Component, NgZone, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { contactCollection } from '../../../api/server/collections/contact';
import { supplierCollection } from '../../../api/server/collections/supplier';
import { Contact } from '../../../api/server/models/contact';
import { Room } from '../../../api/server/models/room';
import { Supplier } from '../../../api/server/models/supplier';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {
  suppliers: Array<Supplier>;
  selectedSupplier: Supplier;
  supplierFlag = true;
  newSupplier = '';
  contacts: Array<Supplier>;
  selectedContact: Supplier;
  contactFlag = true;
  newContact = '';
  constructor(
    private zone: NgZone
  ) { }

  ngOnInit(): void {
    MeteorObservable.subscribe('supplier')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.suppliers = supplierCollection
              .find({})
              .fetch();
            if (this.suppliers.length !== 0) {
              if (this.supplierFlag) {
                this.selectedSupplier = this.suppliers[0];
                this.supplierFlag = false;
              } else if (!this.supplierFlag) {
                const index = this.suppliers.findIndex(supplier => supplier._id === this.selectedSupplier._id);
                this.selectedSupplier = this.suppliers[index];
              }
            }
          });
        });
      });
    MeteorObservable.subscribe('contact')
      .subscribe(() => {
        Tracker.autorun(() => {
          this.zone.run(() => {
            this.contacts = contactCollection
              .find({})
              .fetch();
            if (this.contacts.length !== 0) {
              if (this.contactFlag) {
                this.selectedContact = this.contacts[0];
                this.contactFlag = false;
              } else if (!this.contactFlag) {
                const index = this.contacts.findIndex(contact => contact._id === this.selectedContact._id);
                this.selectedContact = this.contacts[index];
              }
            }
          });
        });
      });
  }
  selectSupplier(supplierId): void {
    const index = this.suppliers.findIndex(supplier => supplier._id === supplierId);
    this.selectedSupplier = this.suppliers[index];
  }
  moveToContact(contact: Contact): void {
    MeteorObservable.call('removeContactFromSupplier', this.selectedSupplier, contact)
      .subscribe();

    if (this.suppliers.length >= 0) {
      this.selectedSupplier = this.suppliers[0];
    } else {
      this.selectedSupplier = {};
    }
  }
  moveToSupplierContact(contactId): void {
    const supplierIndex = this.suppliers.findIndex(house => house._id === this.selectedSupplier._id);
    const contactIndex = this.contacts.findIndex(contact => contact._id === contactId);
    MeteorObservable.call('addContactInSupplier', this.suppliers[supplierIndex], this.contacts[contactIndex])
      .subscribe();
  }
  deleteSupplier(): void {
    if (this.selectedSupplier) {
      MeteorObservable.call('deleteSupplier', this.selectedSupplier)
        .subscribe();
      this.supplierFlag = true;
    }
  }
  addSupplier(): void {
    if (this.newSupplier !== '') {
      const newSupplier = {name: this.newSupplier, contacts: []};
      MeteorObservable.call('addSupplier', newSupplier)
        .subscribe();
      this.newSupplier = '';
    }
  }

  selectContact(contactId): void {
    const index = this.contacts.findIndex(contact => contact._id === contactId);
    this.selectedContact = this.contacts[index];
  }
  deleteContact(): void {
    if (this.selectedContact) {
      MeteorObservable.call('deleteContact', this.selectedContact)
        .subscribe();
      this.contactFlag = true;
    }
  }
  addContact(): void {
    if (this.newContact !== '') {
      const newContact = {name: this.newContact};
      MeteorObservable.call('addContact', newContact)
        .subscribe();
      this.newContact = '';
    }
  }
}
