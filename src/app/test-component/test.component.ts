import { Component, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { todosCollection } from '../../../api/server/collections/collections';
import { Todo } from '../../../api/server/models/models';

@Component({
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  title = 'AngularCLI Meteor Boilerplate';
  todos: Observable<Array<Todo>>;
  todoContent: string;

  ngOnInit(): void {
    this.todos = MeteorObservable.subscribe('todos')
      .pipe(switchMap(() => todosCollection.find()));
  }

  addTodo($event: MouseEvent): void {
    $event.preventDefault();
    MeteorObservable.call('addTodo', this.todoContent)
      .subscribe(
        res => {
          // handle response.
        },
        err => console.error(err)
      );
  }
}
